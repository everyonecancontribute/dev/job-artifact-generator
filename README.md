# Job Artifact Generator

This project runs CI/CD pipelines that generate job artifacts with different sizes and random content. The generated data can be used to analyse storage usage, and test scripts to delete and/or observe the storage usage. 

The goal is to use native GitLab CI/CD and Linux container features and avoid external script dependencies. 

## Usage

1. Include the configuration as remote file in the GitLab CI/CD configuration file. 

```yaml
include:
    - remote: https://gitlab.com/gitlab-de/use-cases/efficiency/job-artifact-generator/-/raw/main/.gitlab-ci.yml
```

Alternatively, copy the [.gitlab-ci.yml](.gitlab-ci.yml) file, or fork the project.

1. Optional: Customize the configuration by overriding the default `generator` job's `parallel:matrix:MB_COUNT` list items. 

```yaml
include:
    - remote: https://gitlab.com/gitlab-de/use-cases/efficiency/job-artifact-generator/-/raw/main/.gitlab-ci.yml

generator:
    parallel:
        matrix:
            - MB_COUNT: [1, 5, 10, 20, 50]
```


1. Re-run pipelines manually, or configure pipeline schedules in `CI/CD > Schedules`. 

### Pipeline Schedules

A daily schedule generates 86 MB of job artifacts, if the default `MB_COUNT` values are set. 

1. Navigate into `CI/CD > Schedules`. 
1. Create a new schedule with the following data
    - Description: `Generate artifacts every day`
    - Interval Pattern: `Custom` and `0 9 * * *` for daily at 9am UTC.
    - Cron Timezone: UTC
    - Target branch: `main`
1. Save and verify 

For testing purposes, you can change the cron interval to `*/5 * * * *` to run the pipeline every five minutes. This is not recommended for production usage over days, weeks, months. 



### Example

An example group is running in https://gitlab.com/gitlab-de/playground/artifact-gen-group 

## Customization 

- Add more jobs similar to the `generator` job.
- Modify the `MB_COUNT` list and add more values. 

```diff
    parallel:
        matrix:
-            - MB_COUNT: [1, 5, 10, 20]
+            - MB_COUNT: [1, 5, 10, 20, 50]
```

## Analysis and deletion

- UI: [Settings > Usage Quotas > Storage](https://docs.gitlab.com/ee/user/usage_quotas.html#view-storage-usage). 
- Automation: GitLab API to list and/or delete job artifacts, for example using the [python-gitlab](https://python-gitlab.readthedocs.io/en/stable/gl_objects/pipelines_and_jobs.html#jobs) library.  

## Insights

The variable `MB_COUNT` is defined by the [parallel matrix](https://docs.gitlab.com/ee/ci/yaml/#parallelmatrix) configuration. The `dd` command reads from the urandom device, and generates block data of 1MB, multiplied by the count. 

```
dd if=/dev/urandom of=`$MB_COUNT`.txt bs=1048576 count=$MB_COUNT
```
